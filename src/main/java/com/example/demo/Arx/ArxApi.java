package com.example.demo.Arx;

import org.deidentifier.arx.*;

import org.deidentifier.arx.Data.DefaultData;

import org.deidentifier.arx.criteria.KAnonymity;

import org.deidentifier.arx.ARXAnonymizer;
import org.deidentifier.arx.ARXConfiguration;

import org.deidentifier.arx.ARXResult;

import org.deidentifier.arx.Data;

import java.util.Arrays;
import java.io.IOException;
import java.util.Iterator;

public class ArxApi {

    AttributeType.Hierarchy.DefaultHierarchy age = null;
    public void anonymization() throws IOException {
        System.out.println("started");
        DefaultData data = Data.create();
        data.add("s,no","email","age",  "name","dob");
        data.add("1","prasad", "0", "81667","81667");
        data.add("1","prasad","34", "81675","81667");
        data.add("1","prasad","34", "81925","81667");
        data.add("1","prasad","0",  "81931","81667");
        data.add("1","prasad","0",  "81931","81667");
        data.add("1","prasad","0","81931","81667");
        data.add("1","prasad","0",  "81931","81667");

        DataHierarchy dataBuilder = new DataHierarchy(data);
        dataBuilder.setDataSourceType(data);
        dataBuilder.initNameBuilder();
        dataBuilder.initEmailBuilder();
        dataBuilder.initDobBuilder();
        dataBuilder.initAgeBuilder();
        dataBuilder.setDataSourceAttributeTypes();

        dataBuilder.setDataSourceHierarchy();

        ARXAnonymizer anonymizer = new ARXAnonymizer();
        ARXConfiguration config = ARXConfiguration.create();
        config.addPrivacyModel(new KAnonymity(3));
        config.setSuppressionLimit(1.0d);
        ARXResult result = anonymizer.anonymize(data, config);
        Iterator<String[]> transformed = result.getOutput(false).iterator();
        while (transformed.hasNext()) {
            System.out.print("   ");
            System.out.println(Arrays.toString(transformed.next()));
        }
    }




}
