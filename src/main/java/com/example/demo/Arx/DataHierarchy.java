package com.example.demo.Arx;

import org.deidentifier.arx.AttributeType;
import org.deidentifier.arx.Data;
import org.deidentifier.arx.DataType;
import org.deidentifier.arx.aggregates.HierarchyBuilder;
import org.deidentifier.arx.aggregates.HierarchyBuilderDate;
import org.deidentifier.arx.aggregates.HierarchyBuilderIntervalBased;
import org.deidentifier.arx.aggregates.HierarchyBuilderRedactionBased;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;

public class DataHierarchy {
    Data dataSource = null;
/* Name
Address
birthdate, admission date, date of death, and age
Phn number
Email address
images profile

 */
    HierarchyBuilderRedactionBased<?> name = null;
    AttributeType.Hierarchy.DefaultHierarchy address = null;
    HierarchyBuilderRedactionBased<?> email = null;
    AttributeType.Hierarchy.DefaultHierarchy phnNumber = null;
    AttributeType.Hierarchy.DefaultHierarchy profilePic = null;

    HierarchyBuilderRedactionBased<?> dob = null;
    AttributeType.Hierarchy.DefaultHierarchy age = null;

    public Data getDataSource() {
        return dataSource;
    }

    DataHierarchy(Data dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataSourceType(Data dataSource) {
        dataSource.getDefinition().setDataType("name", DataType.STRING);
        dataSource.getDefinition().setDataType("email", DataType.STRING);
        dataSource.getDefinition().setDataType("dob", DataType.STRING);
        dataSource.getDefinition().setDataType("age", DataType.DECIMAL);
//        dataSource.getDefinition().setDataType("address", DataType.STRING);
//        dataSource.getDefinition().setDataType("phnNumber", DataType.STRING);
//        dataSource.getDefinition().setDataType("profilePic", DataType.STRING);

    }



    public void initNameBuilder() {
        this.name = HierarchyBuilderRedactionBased.create(HierarchyBuilderRedactionBased.Order.RIGHT_TO_LEFT,
                HierarchyBuilderRedactionBased.Order.RIGHT_TO_LEFT, ' ', '*');
        this.name.setAlphabetSize(40, 36);
    }

    public void initEmailBuilder() {
        System.out.println("data came in");
        this.email = HierarchyBuilderRedactionBased.create(HierarchyBuilderRedactionBased.Order.RIGHT_TO_LEFT,
                HierarchyBuilderRedactionBased.Order.RIGHT_TO_LEFT, ' ', '*');
        this.email.setAlphabetSize(40, 36);

    }



    public void initDobBuilder() {
        System.out.println("data came in");
        this.dob = HierarchyBuilderRedactionBased.create(HierarchyBuilderRedactionBased.Order.RIGHT_TO_LEFT,
                HierarchyBuilderRedactionBased.Order.RIGHT_TO_LEFT, ' ', '*');
        this.dob.setAlphabetSize(40, 36);

    }

    public void initAgeBuilder() {
       this.age =  AttributeType.Hierarchy.create();
        this.age.add("0", "<20", "*");
        this.age.add("34", "<50", "#");
        this.age.add("45", "<50", "*");
        this.age.add("66", ">=50", "*");
        this.age.add("70", ">=50", "*");

    }

    public void setDataSourceAttributeTypes() {
       this.dataSource.getDefinition().setAttributeType("name", AttributeType.QUASI_IDENTIFYING_ATTRIBUTE);
        this.dataSource.getDefinition().setAttributeType("email", AttributeType.QUASI_IDENTIFYING_ATTRIBUTE);
        this.dataSource.getDefinition().setAttributeType("dob", AttributeType.QUASI_IDENTIFYING_ATTRIBUTE);
        this.dataSource.getDefinition().setAttributeType("age", AttributeType.QUASI_IDENTIFYING_ATTRIBUTE);


//        this.dataSource.getDefinition().setAttributeType("address", AttributeType.QUASI_IDENTIFYING_ATTRIBUTE);
//        this.dataSource.getDefinition().setAttributeType("phnNumber", AttributeType.QUASI_IDENTIFYING_ATTRIBUTE);
//        this.dataSource.getDefinition().setAttributeType("profilePic", AttributeType.QUASI_IDENTIFYING_ATTRIBUTE);


    }

    public void setDataSourceHierarchy() {
        this.dataSource.getDefinition().setHierarchy("name", this.name);
        this.dataSource.getDefinition().setHierarchy("email", this.email);
        this.dataSource.getDefinition().setHierarchy("dob", this.dob);
        this.dataSource.getDefinition().setHierarchy("age", this.age);
//        this.dataSource.getDefinition().setHierarchy("address", this.address);
//        this.dataSource.getDefinition().setHierarchy("phnNumber", this.phnNumber);
//        this.dataSource.getDefinition().setHierarchy("profilePic", this.profilePic);
    }
}
