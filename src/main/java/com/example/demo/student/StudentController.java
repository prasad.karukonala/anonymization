package com.example.demo.student;

import com.example.demo.Anonymizer.DataAnonymizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping( path="api/v1/students")
public class StudentController {

    @Autowired
    DataAnonymizer dataAnonymizer;


    @RequestMapping(value = "/anonymize", method = RequestMethod.POST)
    public void anonymize() {
        dataAnonymizer.anonymize();
    }
}

