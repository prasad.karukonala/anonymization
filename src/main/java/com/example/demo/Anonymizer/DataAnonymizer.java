package com.example.demo.Anonymizer;

import com.example.demo.Arx.ArxApi;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.springframework.stereotype.Service;


@Service
public class DataAnonymizer {

    public void anonymize(){
        StructType carSchema = new StructType(
                new StructField[] { new StructField("id", DataTypes.LongType, true, Metadata.empty()),
                        new StructField("name", DataTypes.StringType, true, Metadata.empty()),
                        new StructField("email", DataTypes.StringType, true, Metadata.empty()),
                        new StructField("dob", DataTypes.DateType, true, Metadata.empty()),
                        new StructField("age", DataTypes.IntegerType, true, Metadata.empty()),

                });
        System.out.println("data");
        anonymizePartitions("inputFileNameList");
    }
    public void createRunnables(
            ArxApi arxApi
    ) {
        System.out.println("Reached");
            try {
                arxApi.anonymization();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
    }
    public void anonymizePartitions(String inputFileNameList) {
        ArxApi arxApi = new ArxApi();
        try {
            createRunnables( arxApi);

        } catch (Exception e) {
            System.out.println(e.getMessage());

        }
    }
}
